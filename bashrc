# ============================================================================
#     FILE:  .bashrc
#   AUTHOR:  Julie Engel (jeng) jeng@null.net
#  CREATED:  first version was somewhen in 2010
# ============================================================================

# Source other files if available ============================================#{{{
# TODO:     could be shortened to $FILES = /etc/file1 and then 
#           for file in $files check if file exists and source

# if we have global options, source them
if [ -f /etc/bashrc ]; then
    source /etc/bashrc
fi

# my company works with clearcase, don't judge me

if [ -f /etc/clearcase/profile.sh ]; then
    source /etc/clearcase/profile.sh
fi

if [ -f /etc/clearcase/aliases.sh ]; then
    source /etc/clearcase/aliases.sh
fi

#}}}

# Just for funsies: start every term with figlet(hostname)
if [ -f /usr/bin/figlet ]; then
    figlet -c $(hostname)
fi

# Global variables ===========================================================#{{{

# add $HOME/bin to the start of path
PATH="~/bin:$PATH"

# arduino
PATH="$PATH:/usr/local/avr/bin"

#unset LS_COLORS
LS_COLORS='di=1:fi=0:ln=31:pi=5:so=5:bd=5:cd=5:or=31:mi=0:ex=35:*.rpm=90'

export PATH
export LS_COLORS
export EDITOR=vim
export WINEDITOR=gvim
export SS7HOME=/cc/vobs/STS/TCU/ss7

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
export HISTIGNORE="ls:exit:logout:cd:pwd:reload:fg:cless"

# append to the history file, don't overwrite it
shopt -s histappend

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

#}}}
# Bash Bindings ==============================================================#{{{
#
# For those who want to use Vi bindings in bash, this corrects a few annoyances:
#
#   1) up and down arrows retrieve history lines even in insert mode
#   2) left and right arrows work in insert mode
#   3) Ctrl-A and Ctrl-E work how you expect if you have had to live 
#       in Emacs mode in the past.
#   4) So does Ctrl-D.
#   5) And Ctrl-L.

## Command-mode bindings
# Ctrl-A or Home: insert at line beginning like in emacs mode
bind -m vi-command 'Control-a: vi-insert-beg'
# Ctrl-E or End: append at line end like in emacs mode
bind -m vi-command 'Control-e: vi-append-eol'
# to switch to emacs editing mode
bind -m vi-command '"ZZ": emacs-editing-mode'

## Insert-mode bindings
# up arrow or PgUp: append to previous history line
bind -m vi-insert '"\M-[A": ""'
bind -m vi-insert '"\M-[5~": ""'
bind -m vi-insert 'Control-p: previous-history'
# dn arrow or PgDn: append to next history line
bind -m vi-insert '"\M-[B": ""'
bind -m vi-insert '"\M-[6~": ""'
bind -m vi-insert 'Control-n: next-history'
# Ctrl-A: insert at line start like in emacs mode
bind -m vi-insert 'Control-a: beginning-of-line'
# Ctrl-E: append at line end like in emacs mode
bind -m vi-insert 'Control-e: end-of-line'
# Ctrl-D: delete character
bind -m vi-insert 'Control-d: delete-char'
# Ctrl-L: clear screen
bind -m vi-insert 'Control-l: clear-screen'

## Emacs bindings
# Meta-V: go back to vi editing
bind -m emacs '"\ev": vi-editing-mode'

## Specify vi editing mode
set -o vi

#}}}
# Aliases ====================================================================#{{{

alias reload='source ~/.bashrc'

# i'm lazy; abbreviate most used stuff----------------------------------------
alias :q="exit"
alias q="exit"
alias c='clear'
alias j='jobs'
alias vi="vim"
alias lp='leafpad'
alias mp='mousepad'
alias g="git"
alias gs="git status"
alias gd="git diff"
alias gls='find $HOME -name ".git"'
alias m="make"
alias mr="make release"
alias mc="make clean"

# some general usage shortcuts -----------------------------------------------
#
alias xterm='xterm +sb'
alias urxvt='urxvt +sb'
alias ls="ls --color --time-style=long-iso"
alias l.='ls -dF .[a-zA-Z0-9]*' #only show dotfiles
alias pgrep='pgrep -fl'
alias mount='mount |column -t'
alias screen='screen -T xterm-256color -xR'
alias screen-retach='screen -raAd'
alias lsscr='screen -list'
alias cal='cal -m3'
alias today='date +"%A, %B %-d, %Y"'

#alias rdesktop='rdesktop -k de-ch -g 1280x1024'

# !!! company conf dependen shortcuts
alias mutt='mutt -f imap://engelju@poseidon.nexus-ag.com'
alias repo-co="svn co http://ilum/svn/netview/engelju"
alias svn-log='svn log -v'

alias mocp="mocp -T lianli"
alias scrot='import -window root screenshot.png'

#function grep() {
    if [ -f $HOME/bin/ack ]; then
        #alias grep='~/bin/ack --color-filename=red color-lineno=black'
        #$HOME/bin/ack $@ --color-filename=red --color-lineno=black
        alias grep='$HOME/bin/ack'
    else
        #alias grep='grep -rIin --color'
        grep -rIin --color
    fi
#}

if [ -f $HOME/bin/chromer/chrome-wrapper ]; then
    alias chrome="$HOME/bin/chromer/chrome-wrapper"
fi

# text file conv
alias dos2unix='recode dos/CR-LF..l1'
alias unix2win='recode l1..windows-1250'
alias unix2dos='recode l1..dos/CR-LF'

#}}}
# Prompt modifications =======================================================#{{{

# Parse if Git directory is dirty --------------------------------------------
function parse_git_dirty {
    [[ $(git status 2> /dev/null | tail -n1) != "nothing to commit (working directory clean)" ]] && echo "*"
}

# Parse Git branch + dirty indicator -----------------------------------------
function parse_git_branch {
    git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \(.*\)/[\1$(parse_git_dirty)]/"
}

# Parse current ClearCase view -----------------------------------------------
function get_clearcase_view {
    wv=$(cleartool pwv |grep "Set view:")

    if [[ $wv == *NONE* ]]; then
        clearcaseview="no view set";
    else
        tmp=${wv#*:}
        clearcaseview=${tmp#*: }
    fi
    # echo clearcaseview
    export $clearcaseview
}

# Get current width of terminal ----------------------------------------------
function prompt_command {

    TERMWIDTH=${COLUMNS}

    #   Add all the accessories below ...
    local temp="┌──[${usernam}@${hostnam}:${cur_tty}]──[${PWD}]──"

    let fillsize=${TERMWIDTH}-${#temp}

    if [ "$fillsize" -gt "0" ]; then
        fill="───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────"
        #   It's theoretically possible someone could need more 
        #   dashes than above, but very unlikely!  HOWTO users, 
        #   the above should be ONE LINE, it may not cut and
        #   paste properly
        fill="${fill:0:${fillsize}}"
        newPWD="${PWD}"
    fi

    if [ "$fillsize" -lt "0" ]; then
        fill=""
        let cut=3-${fillsize}
        newPWD="...${PWD:${cut}}"
    fi
}

get_clearcase_view
PROMPT_COMMAND=prompt_command

hostnam=$(hostname)
usernam=$(whoami)
temp="$(tty)"
#   Chop off the first five chars of tty (ie /dev/):
cur_tty="${temp:5}"
unset temp

LIGHT_GRAY="\[\033[0;37m\]"
NO_COLOUR="\[\033[0m\]"

# While were at it, change xterm title bar------------------------------------

case $TERM in
    xterm*|rxvt*)
        TITLEBAR='\[\033]0;\u@\h:\w\007\]'
        ;;
    *)
        TITLEBAR=""
        ;;
esac

# And now change PS1 & PS2 to it ---------------------------------------------

if [ ! -n "$SSH_TTY" ]; then                # local login
    PS1="$TITLEBAR\
$LIGHT_GRAY┌──[\
$NO_COLOUR\$usernam$LIGHT_GRAY@$NO_COLOUR\$hostnam$NO_COLOUR:${cur_tty}\
${LIGHT_GRAY}]─\${fill}─[\
$NO_COLOUR\${newPWD}\
$LIGHT_GRAY]──\
\n$LIGHT_GRAY└─>[$NO_COLOUR$clearcaseview$LIGHT_GRAY]──$NO_COLOUR\\$ " 
#\n$LIGHT_GRAY└─>[$NO_COLOUR$(parse_git_branch)$LIGHT_GRAY]──$NO_COLOUR\\$ " 

    PS2="$NO_COLOUR-$LIGHT_GRAY-$LIGHT_GRAY-$NO_COLOUR "
    export PS1
    export PS2
else                                        #ssh login
    export PS1="\u@\h:\W "
fi

#}}}
# Shell Functions ============================================================#{{{

# Creates an archive from a given directory ----------------------------------
mktar() { tar cvf  "${1%%/}.tar"     "${1%%/}/"; }
mktgz() { tar cvzf "${1%%/}.tar.gz"  "${1%%/}/"; }
mktbz() { tar cvjf "${1%%/}.tar.bz2" "${1%%/}/"; }

# Extracts the given directory according to its ending -----------------------
extract () {
  if [ -f $1 ] ; then
      case $1 in
          *.tar.bz2)   tar xvjf $1    ;;
          *.tar.gz)    tar xvzf $1    ;;
          *.bz2)       bunzip2 $1     ;;
          *.rar)       rar x $1       ;;
          *.gz)        gunzip $1      ;;
          *.tar)       tar xvf $1     ;;
          *.tbz)       tar -xjf $1    ;;
          *.tbz2)      tar xvjf $1    ;;
          *.tgz)       tar xvzf $1    ;;
          *.zip)       unzip $1       ;;
          *.Z)         uncompress $1  ;;
          *.7z)        7z x $1        ;;
          *)           echo "don't know how to extract '$1'..." ;;
      esac
  else
      echo "'$1' is not a valid file!"
  fi
}

# Prefix any item in the current directory with the given prefix -------------
prefix () {
    for x in * ; do 
    	mv "$x" "$1$x" ;
    done
}

# Make a copy of the given file with the current datetime attached -----------
backup () {
    cp $1 `basename $1`-`date +%Y%m%d`.backup ;
}

# Unused functions -----------------------------------------------------------#{{{

#test if a file should be opened normally, or as root (vi)
#argc () {
#       count=0;
#       for arg in "$@"; do
#              if [[ ! "$arg" =~ '-' ]]; then count=$(($count+1)); fi;
#       done;
#       echo $count;
#}

#vi () { if [[ `argc "$@"` > 1 ]]; then /usr/bin/vim $@;
#                elif [ $1 = '' ]; then /usr/bin/vim;
#                elif [ ! -f $1 ] || [ -w $1 ]; then /usr/bin/vim $@;
#                else
#                        echo -n "File is readonly. Edit as root? (Y/n): "
#                        read -n 1 yn; echo;
#                        if [ "$yn" = 'n' ] || [ "$yn" = 'N' ];
#                            then /usr/bin/vim $*;
#                            else su -c "/usr/bin/vim $*";
#                        fi
#                fi
#            }

#cat () {
  #/usr/local/bin/spc $1
  #RETVAL=$?
  #[ $RETVAL -ne 0 ] && /bin/cat $1
  #[ $RETVAL -eq 1 ] && /usr/local/bin/spc $1
#}
#}}}
#}}}
# Handy dandy internet shortcuts =============================================#{{{
# Best served with w3m as textbased internet browser

textbrowser=w3m

# Google ---------------------------------------------------------------------#{{{
google() {
    search=""
    for term in $*; do
        search="$search%20$term"
    done
    exec $textbrowser "http://www.google.com/search?q=$search"
}
#}}}
# Leo, excellent GER-ENG dictionary ------------------------------------------#{{{
leo() {
    search=""
    for term in $*; do
        search="$search%20$term"
    done
    $textbrowser "http://pda.leo.org/ende?lp=ende&lang=de&searchLoc=0&cmpType=relaxed&sectHdr=on&spellToler=on&chinese=both&pinyin=diacritic&search=$search&relink=on"
}
#}}}
# German wikipedia searching -------------------------------------------------#{{{
wiki() {
    search=""
    for term in $*; do
        search="$search%20$term"
    done
    $textbrowser "http://de.m.wikipedia.org/wiki/$search"
}
#}}}
# English wikipedia searching ------------------------------------------------#{{{
wiki-en() {
    search=""
    for term in $*; do
        search="$search%20$term"
    done
    $textbrowser "http://en.m.wikipedia.org/wiki/$search"
}
#}}}
# Ubuntu users (german ubuntu wiki) searching --------------------------------#{{{
uusers() {
    search=""
    for term in $*; do
        search="$search%20$term"
    done
    $textbrowser "http://wiki.ubuntuusers.de/$search"
}
#}}}
# Search stackoverflow -------------------------------------------------------#{{{
soflow() {
    search=""
    "$1"
    for term in $*; do
        search="$search+$term"
    done
    $textbrowser "http://sa.column80.com/?s=$search&t=intitle"
}
#}}}
#}}}
