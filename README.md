dotfiles
========

On a new computer, clone the repository.

Add a new dotfile to the repository:
    mv ~/.dotfile ~/dotfiles/dotfile

Link the dotfile from the repo to the homedir
    ln -s ~/dotfiles/dotfile ~/.dotfile
